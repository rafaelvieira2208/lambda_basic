provider "aws" {
  region = "us-east-1"
}

module "lambda_function" {
  source = "../"
  function_name = "myfunctiontest"
  publish = true
  filename                       =  "package.zip"
  handler                        = "lambda-hello-world"
  memory_size                    = 256
  reserved_concurrent_executions = -1
  runtime                        = "dotnetcore3.1"
  timeout                        = 15
   tags = {
    CC = "E-commerce"
  }
}

resource "aws_lambda_event_source_mapping" "sqs_trigger" {
  event_source_arn = "arn:aws:sqs:us-east-1:665702740429:DEV_INTEGRACOES_ACTIVATION_DISCOUNT_CEAPAY"
  function_name    = module.lambda_function.lambda_function_name
}