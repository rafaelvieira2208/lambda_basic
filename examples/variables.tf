# variable "function_name" {
#   description = "A unique name for your Lambda Function"
#   type        = string
#   default     = "myfunctionTestLambda"
# }

variable "cloudwatch_logs_retention_in_days" {
  description = "Specifies the number of days you want to retain log events in the specified log group. Possible values are: 1, 3, 5, 7, 14, 30, 60, 90, 120, 150, 180, 365, 400, 545, 731, 1827, and 3653."
  type        = number
  default     = 1
}